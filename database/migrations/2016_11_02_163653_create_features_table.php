<?php

use Illuminate\Support\Facades\Schema;
use Phaza\LaravelPostgis\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeaturesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('features', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name');
            $table->string('address');
            $table->string('slug');
            $table->text('description');
            $table->point('location');
            $table->polygon('extent')->nullable();
            $table->string('wikidata')->nullable();
            $table->string('data_source_feature_identifier')->nullable();
            $table->integer('data_source_id')->unsigned();
            $table->foreign('data_source_id')->references('id')->on('data_sources')->onDelete('cascade');
            $table->integer('feature_type_id')->unsigned();
            $table->foreign('feature_type_id')->references('id')->on('feature_types')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('features');
    }
}
