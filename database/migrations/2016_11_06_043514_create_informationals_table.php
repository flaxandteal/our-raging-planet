<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInformationalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('informationals', function (Blueprint $table) {
            $table->increments('id');

            $table->string('text');
            $table->string('detail')->default('');
            $table->string('class')->default('notification');
            $table->string('author')->default('OurRagingPlanet');
            $table->string('metadata')->default('{}');
            $table->integer('timeOffset')->default(0);
            $table->integer('simulation_id')->unsigned();
            $table->foreign('simulation_id')->references('id')->on('simulations')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('informationals');
    }
}
