<?php

use Phaza\LaravelPostgis\Geometries\LineString;
use Phaza\LaravelPostgis\Geometries\Point;
use Phaza\LaravelPostgis\Geometries\Polygon;
use GeoJson\GeoJson;
use Illuminate\Database\Seeder;
use Cocur\Slugify\Slugify;

class IndustrialHeritageRecordSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $data_source = App\DataSource::whereName('NI Industrial Heritage Record')->first();

      if ($data_source)
          $data_source->features()->delete();
      else
          DB::table('data_sources')->insert([
            'name' => 'NI Industrial Heritage Record',
            'owner' => 'Department of Communities',
            'license_title' => 'UK-OGL',
            'license_url' => 'http://www.nationalarchives.gov.uk/doc/open-government-licence/version/3/',
            'uri' => 'https://www.opendatani.gov.uk/dataset/industrial-heritage-record'
          ]);

      $data_source_id = App\DataSource::whereName('NI Industrial Heritage Record')->first()->id;
      $feature_type_id = App\FeatureType::whereName('Industrial Heritage')->first()->id;

      $indher_json = json_decode(file_get_contents(base_path() . '/resources/opendata/industrial-heritage-record.geojson'));

      $indhers = GeoJson::jsonUnserialize($indher_json);
      $slugify = new Slugify();
      foreach ($indhers as $indher) {
          $coordinates = $indher->getGeometry()->getCoordinates();

          $feature = new App\Feature;
          $properties = $indher->getProperties();
          $placename = strtolower($properties['TD']);
          $feature->name = $properties['X_TYPE'] . ' at ' . $placename;
          $feature->address = $properties['LOCATION'] . ', ' . $placename . ', ' . $properties['CO'];
          $feature->slug = $slugify->slugify('ihr-' . $properties['LOCATION']. '-' . $properties['IHR']);
          $location = $properties['LOCATION'];
          if (!$location)
              $location = $placename;
          $feature->description = 'Part of our industrial history, this ' . $properties['X_TYPE'] . ' is located at ' . $location;
          $feature->location = new Point($coordinates[1], $coordinates[0]);
          $feature->data_source_feature_identifier = $properties['IHR'];

          $feature->data_source_id = $data_source_id;
          $feature->feature_type_id = $feature_type_id;

          $feature->save();
      }
    }
}
