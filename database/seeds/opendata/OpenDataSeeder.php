<?php

use Illuminate\Database\Seeder;

class OpenDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $this->call(AreasOfArchaeologicalPotentialSeeder::class);
         $this->call(IndustrialHeritageRecordSeeder::class);
         $this->call(ActivePlacesNISeeder::class);
         $this->call(LightingAssetsSeeder::class);
         $this->call(BusStopsSeeder::class);
    }
}
