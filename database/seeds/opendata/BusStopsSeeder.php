<?php

use Phaza\LaravelPostgis\Geometries\LineString;
use Phaza\LaravelPostgis\Geometries\Point;
use Phaza\LaravelPostgis\Geometries\Polygon;
use GeoJson\GeoJson;
use Illuminate\Database\Seeder;
use Cocur\Slugify\Slugify;

class BusStopsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $data_source = App\DataSource::whereName('Translink Bus Stop List')->first();

      if ($data_source)
          $data_source->features()->delete();
      else
          DB::table('data_sources')->insert([
            'name' => 'Translink Bus Stop List',
            'owner' => 'Translink',
            'license_title' => 'UK-OGL',
            'license_url' => 'http://www.nationalarchives.gov.uk/doc/open-government-licence/version/3/',
            'uri' => 'https://www.opendatani.gov.uk/dataset/translink-bus-stop-list'
          ]);

      $data_source_id = App\DataSource::whereName('Translink Bus Stop List')->first()->id;
      $feature_type_id = App\FeatureType::whereName('Bus Stop')->first()->id;

      $translink_json = json_decode(file_get_contents(base_path() . '/resources/opendata/bus-stop-list-february-2016.geojson.json'));

      $translink_bus_stops = GeoJson::jsonUnserialize($translink_json);
      $slugify = new Slugify();
      foreach ($translink_bus_stops as $bus_stop) {
          $coordinates = $bus_stop->getGeometry()->getCoordinates();

          $feature = new App\Feature;
          $properties = $bus_stop->getProperties();
          $feature->name = $properties['Stop_Name'] . ' Bus Stop';
          $feature->address = $properties['Stop_Name'];
          $feature->slug = $slugify->slugify('bs-' . $feature->name . '-' . $properties['LocationID']);
          $feature->description = 'Translink Bus Stop';
          $feature->location = new Point($coordinates[1], $coordinates[0]);
          $feature->data_source_feature_identifier = $properties['LocationID'];

          $feature->data_source_id = $data_source_id;
          $feature->feature_type_id = $feature_type_id;

          $feature->save();
      }
    }
}
