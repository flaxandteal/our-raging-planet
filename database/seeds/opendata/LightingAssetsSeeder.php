<?php

use Phaza\LaravelPostgis\Geometries\LineString;
use Phaza\LaravelPostgis\Geometries\Point;
use Phaza\LaravelPostgis\Geometries\Polygon;
use GeoJson\GeoJson;
use Illuminate\Database\Seeder;
use Cocur\Slugify\Slugify;

class LightingAssetsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $data_source = App\DataSource::whereName('TransportNI Lighting Assets')->first();

      if ($data_source)
          $data_source->features()->delete();
      else
          DB::table('data_sources')->insert([
            'name' => 'TransportNI Lighting Assets',
            'owner' => 'TransportNI',
            'license_title' => 'UK-OGL',
            'license_url' => 'http://www.nationalarchives.gov.uk/doc/open-government-licence/version/3/',
            'uri' => 'https://www.opendatani.gov.uk/dataset/lighting-assets'
          ]);

      $data_source_id = App\DataSource::whereName('TransportNI Lighting Assets')->first()->id;
      $feature_type_id = App\FeatureType::whereName('Street Light')->first()->id;

      $lighting_json = json_decode(file_get_contents(base_path() . '/resources/opendata/lightingpointssouthern.geojson'));

      $assets = GeoJson::jsonUnserialize($lighting_json);
      $slugify = new Slugify();
      $counter = 0;
      $counter_total = count($assets);
      foreach ($assets as $asset) {
          $coordinates = $asset->getGeometry()->getCoordinates();

          $feature = new App\Feature;
          $properties = $asset->getProperties();
          $feature->name = 'Street Light';
          $feature->address = $properties['STREET_DESCRIPTOR'];
          if (!$feature->address)
              $feature->address = "Streetside";

          $feature->slug = $slugify->slugify('la-' . $properties['ITEM_IDENTITY_CODE']);
          $feature->description = 'Lighting for pedestrians, cars and other users';
          $feature->location = new Point($coordinates[1], $coordinates[0]);
          $feature->data_source_feature_identifier = $properties['ITEM_IDENTITY_CODE'];

          $feature->data_source_id = $data_source_id;
          $feature->feature_type_id = $feature_type_id;

          $feature->save();

          $counter++;
          if ($counter % 1000 == 0)
              $this->command->info("Added {$counter}/{$counter_total} lighting assets");
      }
    }
}
