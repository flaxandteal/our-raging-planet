<?php

use Phaza\LaravelPostgis\Geometries\LineString;
use Phaza\LaravelPostgis\Geometries\Point;
use Phaza\LaravelPostgis\Geometries\Polygon;
use GeoJson\GeoJson;
use Illuminate\Database\Seeder;
use Cocur\Slugify\Slugify;

class ActivePlacesNISeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

      $data_source = App\DataSource::whereName('Active Places NI')->first();

      if ($data_source)
          $data_source->features()->delete();
      else
          DB::table('data_sources')->insert([
            'name' => 'Active Places NI',
            'owner' => 'Sport NI',
            'license_title' => 'UK-OGL',
            'license_url' => 'http://www.nationalarchives.gov.uk/doc/open-government-licence/version/3/',
            'uri' => 'https://www.opendatani.gov.uk/dataset/active-places-ni-sports-facilities-database/resource/9615b5b6-3f11-4968-b337-f1dc1e9db828'
          ]);

      $data_source_id = App\DataSource::whereName('Active Places NI')->first()->id;
      $feature_type_id = App\FeatureType::whereName('Sports Facility')->first()->id;

      $actplas = json_decode(file_get_contents(base_path() . '/resources/opendata/active-places-ni-converted.json'), true);
      $slugify = new Slugify();
      $n = 0;
      foreach ($actplas as $place) {
          $n++;
          $feature = new App\Feature;
          $feature->name = $place['VENUE_NAME'];
          $feature->address = $place['ADDRESS_LINE_1'] . ', ' . $place['POST_TOWN'] . ', ' . $place['COUNTY'] . ', ' . $place['POST_CODE'];
          $feature->slug = $slugify->slugify('ap-' . $place['VENUE_NAME'] . '-' . $place['POST_TOWN']);
          $provides = [];

          foreach ($place as $k => $v)
              if ($v == 'Yes')
                  $provides[] = ucwords(str_replace('_', ' ', $k));

          $provides = implode($provides, ', ');

          $feature->description = trim(<<<ENDDESC
              A sporting/exercise facility, {$place['VENUE_NAME']}, provides: {$provides}
ENDDESC
          );
          $feature->location = new Point($place['lat'], $place['lng']);
          $feature->data_source_feature_identifier = $n;

          $feature->data_source_id = $data_source_id;
          $feature->feature_type_id = $feature_type_id;

          $feature->save();
      }
    }
}
