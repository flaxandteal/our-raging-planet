<?php

use Phaza\LaravelPostgis\Geometries\LineString;
use Phaza\LaravelPostgis\Geometries\Point;
use Phaza\LaravelPostgis\Geometries\Polygon;
use GeoJson\GeoJson;
use Illuminate\Database\Seeder;
use Cocur\Slugify\Slugify;

class AreasOfArchaeologicalPotentialSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $data_source = App\DataSource::whereName('NI Areas of Archaeological Potential')->first();

      if ($data_source)
          $data_source->features()->delete();
      else
          DB::table('data_sources')->insert([
            'name' => 'NI Areas of Archaeological Potential',
            'owner' => 'Department of Communities',
            'license_title' => 'UK-OGL',
            'license_url' => 'http://www.nationalarchives.gov.uk/doc/open-government-licence/version/3/',
            'uri' => 'https://www.opendatani.gov.uk/dataset/areas-of-archaeological-potential'
          ]);

      $data_source_id = App\DataSource::whereName('NI Areas of Archaeological Potential')->first()->id;
      $feature_type_id = App\FeatureType::whereName('Unexplored Cultural Heritage')->first()->id;

      $archpot_json = json_decode(file_get_contents(base_path() . '/resources/opendata/areas-of-archaeological-potential.geojson'));

      $archpots = GeoJson::jsonUnserialize($archpot_json);
      $slugify = new Slugify();
      foreach ($archpots as $archpot) {
          $coordinates = $archpot->getGeometry()->getCoordinates()[0][0][0];
          print_r($coordinates);

          $feature = new App\Feature;
          $properties = $archpot->getProperties();
          $placename = ucwords(strtolower($properties['LOCATION']));
          $feature->name = $placename . ' Archaeological Interest';
          $feature->address = $placename . ', ' . ucwords(strtolower($properties['LGD']));
          $feature->slug = $slugify->slugify('aap-' . $properties['LOCATION']. '-' . $properties['OBJECTID']);
          $feature->description = 'Archaeology hidden, for the moment, under ' . $placename . '. When areas are redeveloped, this can be further explored.';
          $feature->location = new Point($coordinates[1], $coordinates[0]);
          $feature->data_source_feature_identifier = $properties['OBJECTID'];

          $feature->data_source_id = $data_source_id;
          $feature->feature_type_id = $feature_type_id;

          $feature->save();
      }
    }
}
