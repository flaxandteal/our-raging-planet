<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $this->call(ORPLaravelPanelLinkSeeder::class);
         $this->call(PhenomenonsTableSeeder::class);
         $this->call(FeatureTypesTableSeeder::class);

         $this->call(OpenDataSeeder::class);

         $this->call(UsersTableSeeder::class);
         $this->call(FeaturesTableSeeder::class);
         $this->call(SimulationsTableSeeder::class);
    }
}
