<?php

use Phaza\LaravelPostgis\Geometries\LineString;
use Phaza\LaravelPostgis\Geometries\Point;
use Phaza\LaravelPostgis\Geometries\Polygon;
use Illuminate\Database\Seeder;

class FeatureTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('data_sources')->delete();
      DB::table('feature_types')->delete();

      DB::table('feature_types')->insert([
        'name' => 'My School',
        'slug' => 'my-school',
        'description' => 'Where we learn!'
      ]);

      DB::table('feature_types')->insert([
        'name' => 'Bus Stop',
        'slug' => 'bus-stop',
        'description' => 'Pick up and drop off point for buses'
      ]);

      DB::table('feature_types')->insert([
        'name' => 'Street Light',
        'slug' => 'street-light',
        'description' => 'Lighting for street users'
      ]);

      DB::table('feature_types')->insert([
        'name' => 'Unexplored Cultural Heritage',
        'slug' => 'unexplored-cultural-heritage',
        'description' => 'Mysteries of our past waiting to be discovered'
      ]);

      DB::table('feature_types')->insert([
        'name' => 'Industrial Heritage',
        'slug' => 'industrial-heritage',
        'description' => 'Artifact of our industrial past'
      ]);

      DB::table('feature_types')->insert([
        'name' => 'Sports Facility',
        'slug' => 'sports-facility',
        'description' => 'Venue for sports or exercise'
      ]);
    }
}
