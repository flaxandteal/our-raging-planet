<?php

use Serverfireteam\Panel\Link;
use Illuminate\Database\Seeder;

class ORPLaravelPanelLinkSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      Link::whereUrl('Task')->delete();
      $link = new Link();
      $link->getAndSave('Task', 'Tasks');
      $link->save();
    }
}
