<?php

use Illuminate\Database\Seeder;

class PhenomenonsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('phenomenons')->delete();

      DB::table('phenomenons')->insert([
        'name' => 'Volcano',
        'symbol' => "/images/icon-volcano.svg",
        'color' => '#e50000'
      ]);
      DB::table('phenomenons')->insert([
        'name' => 'Earthquake',
        'symbol' => "/images/icon-earthquake.svg",
        'color' => '#c69a19'
      ]);
      DB::table('phenomenons')->insert([
        'name' => 'Storm Surge',
        'symbol' => "/images/icon-flood.svg",
        'color' => '#0074e5'
      ]);
      DB::table('phenomenons')->insert([
        'name' => 'Flashflood',
        'symbol' => "/images/icon-flood.svg",
        'color' => '#0074e5'
      ]);
    }
}
