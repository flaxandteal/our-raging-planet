<?php

use Phaza\LaravelPostgis\Geometries\Point;
use Cocur\Slugify\Slugify;
use Illuminate\Database\Seeder;

class SimulationsTableSeeder extends Seeder
{
    public function attachNearbyFeaturesToSimulation($simulation) {
        $features = App\Feature::whereRaw("ST_DWithin(location, ST_GeomFromText('" . $simulation->center->toWKT() . "'), 5000)")
            ->select('id');


        \Log::info($features->count());
        $features = $features->get();
        App\FeatureState::insert($features->map(function ($f) use ($simulation) {
            return [
                'feature_id' => $f->id,
                'simulation_id' => $simulation->id,
                'health' => 100,
                'summary' => 'All in order',
                'history' => '[]'
            ];
        })->toArray());
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $data_source = App\DataSource::whereName('Teacher')->first();

      if ($data_source)
          $data_source->features()->delete();
      else
          DB::table('data_sources')->insert([
            'name' => 'Teacher',
            'owner' => '(none)',
            'license_title' => '',
            'license_url' => '',
            'uri' => ''
          ]);

      DB::table('simulations')->delete();

      $simulation_location = new Point(54.102, -6.253);

      $simulation = new App\Simulation;
      $simulation->caption = "near Warrenpoint";
      $simulation->center = $simulation_location;
      $simulation->settings = '';

      $simulation->phenomenon()->associate(App\Phenomenon::whereName('Storm Surge')->first());
      $simulation->owner()->associate(App\User::whereName('Test')->first());

      $simulation->save();

      $slugify = new Slugify();
      $placename = 'St Mark\'s High School';
      App\Feature::whereName($placename)->delete();
      $feature = new App\Feature;
      $feature->name = $placename;
      $feature->address = $placename . ', Warrenpoint';
      $feature->slug = $slugify->slugify('mys-' . $placename);
      $feature->description = 'This is our school where we learn!';
      $feature->location = new Point(54.1094, -6.2542);
      $feature->data_source_feature_identifier = $slugify->slugify('mys-' . $placename);

      $feature->data_source_id = $data_source->id;
      $feature->feature_type_id = App\FeatureType::whereName('My School')->first()->id;
      $feature->save();

      $feature_state = new App\FeatureState;

      $feature_state->health = 100;
      $feature_state->summary = "Good condition";
      $feature_state->history = "";
      $feature_state->feature()->associate(App\Feature::whereName($placename)->first());
      $feature_state->simulation()->associate($simulation);

      $feature_state->save();

      $informational = new App\Informational;
      $informational->text = "Flood Warning!";
      $informational->timeOffset = 0;
      $informational->detail = "Water levels predicted to rise steeply in the coming hours";
      $informational->simulation()->associate($simulation);
      $informational->save();

      $informational = new App\Informational;
      $informational->text = "Trending Social Media Post";
      $informational->class = "social-media";
      $informational->author = "@boatergirl2017";
      $informational->timeOffset = 3814;
      $informational->detail = "What's up with the tide?? Why's my car underwater?! #waterworld";
      $informational->metadata = json_encode(['likes' => 251, 'shares' => 19]);
      $informational->simulation()->associate($simulation);
      $informational->save();

      $informational = new App\Informational;
      $informational->text = "News Channel Breaking Headline";
      $informational->timeOffset = 5357;
      $informational->class = 'newsflash';
      $informational->detail = "Warrenpoint scene of heavy flooding - Catastrophic damage anticipated";
      $informational->simulation()->associate($simulation);
      $informational->save();

      $informational = new App\Informational;
      $informational->text = "Park Flooded!";
      $informational->timeOffset = 7200;
      $informational->detail = "Heavy water logging leaving pitch with long-term damage";
      $informational->simulation()->associate($simulation);
      $informational->save();

      $informational = new App\Informational;
      $informational->text = "Trending Social Media Post";
      $informational->class = "social-media";
      $informational->author = "@maxExerciseFan";
      $informational->timeOffset = 7210;
      $informational->detail = "Just got out of Clonallon Park in time! #crazyWeather out there!!";
      $informational->metadata = json_encode(['likes' => 51, 'shares' => 129]);
      $informational->simulation()->associate($simulation);
      $informational->save();

      App\Task::whereHeading("Back on the Ball")->delete();
      $task = new App\Task;
      $task->simulation()->associate($simulation);
      $task->heading = "Back on the Ball";
      $task->description = <<<ENDDESC
      Warrenpoint Park, St Peters GAC and Millburn Park are various elevations and distances from the shoreline. Can you work out roughly how long each is under water for?

      What facilities are at each (e.g. pitches, courts)? How does water affect each of those?

      Move to the end of the simulation and look at the repair times for each. Is this all that is required to make them usable on match-days? What other resources nearby need to be working again also?

      Do you watch sport at these facilities? How would you suggest teams and players make temporary plans until they are repaired?

      In the social media feed, @maxExerciseFan is in Clonallon Park when the flood hits. Using information from the flooding timeline and his post, create a short account for a local newspaper reporting on the disaster.
ENDDESC;
      $task->save();

      App\Task::whereHeading("View from Above")->delete();
      $task = new App\Task;
      $task->simulation()->associate($simulation);
      $task->heading = "View from Above";
      $task->description = <<<ENDDESC
      <center>
          <p>What would Warrenpoint look like from above one day after the flood starts? <strong>HW:</strong> Open the image below in a painting program and illustrate the water level by looking at the map.</p>

          <a title="By Ardfern (Own work) [CC BY-SA 3.0 (http://creativecommons.org/licenses/by-sa/3.0) or GFDL (http://www.gnu.org/copyleft/fdl.html)], via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File%3AWarrenpoint%2C_July_2010_(02).JPG"><img width="400" alt="Warrenpoint, July 2010 (02)" src="https://upload.wikimedia.org/wikipedia/commons/thumb/0/06/Warrenpoint%2C_July_2010_%2802%29.JPG/512px-Warrenpoint%2C_July_2010_%2802%29.JPG"/></a>
      </center>
ENDDESC;
      $task->save();

      $feature_state = new App\FeatureState;

      $feature_state->health = 50;
      $feature_state->summary = "Heavy damage";
      $feature_state->history = "";
      $feature_state->feature()->associate(App\Feature::whereName('Clonallon Park')->first());
      $feature_state->simulation()->associate($simulation);

      $feature_state->save();

      $this->attachNearbyFeaturesToSimulation($simulation);

      $simulation_location = new Point(54.86, -6.28);

      $simulation = new App\Simulation;
      $simulation->caption = "close to Ballymena";
      $simulation->center = $simulation_location;
      $simulation->settings = '';

      $simulation->phenomenon()->associate(App\Phenomenon::whereName('Earthquake')->first());
      $simulation->owner()->associate(App\User::whereName('Test')->first());

      $simulation->save();

      $this->attachNearbyFeaturesToSimulation($simulation);

      $simulation_location = new Point(54.824, -7.464);

      $simulation = new App\Simulation;
      $simulation->caption = "in Strabane";
      $simulation->center = $simulation_location;
      $simulation->settings = '';

      $simulation->phenomenon()->associate(App\Phenomenon::whereName('Volcano')->first());
      $simulation->owner()->associate(App\User::whereName('Test')->first());

      $simulation->save();

      $this->attachNearbyFeaturesToSimulation($simulation);
    }
}
