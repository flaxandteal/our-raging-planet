import pandas as pd
import time
import utils

df = pd.read_csv('active-places-ni.csv')

converter = utils.IrishGridConverter()

def add_loc(row):
    coords = converter(row.loc['EASTING'], row.loc['NORTHING'])
    row['lat'] = coords[0]
    row['lng'] = coords[1]
    return row

df = df.apply(add_loc, axis=1)

df.to_json('active-places-ni-converted.json', orient='records')
