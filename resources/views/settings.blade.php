@extends('layouts.app')

@section('content')

<h1>My Settings</h1>

  <passport-clients></passport-clients>
  <passport-authorize-clients></passport-authorize-clients>
  <passport-personal-access-tokens></passport-personal-access-tokens>

@endsection
