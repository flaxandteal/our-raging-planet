@extends('layouts.app')

@section('content')

@if (session()->has('status'))
    <alert brief>{{ session('status') }}</alert>
@endif

<home-page></home-page>

@endsection
