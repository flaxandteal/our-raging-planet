
/**
 * First we will load all of this project's JavaScript dependencies which
 * include Vue and Vue Resource. This gives a great starting point for
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the body of the page. From here, you may begin adding components to
 * the application, or feel free to tweak this setup for your needs.
 */

import Vuex from 'vuex'
import AsyncComputed from 'vue-async-computed'

Vue.component('app', require('./App.vue'));
Vue.component('home-page', require('./components/HomePage.vue'));
Vue.component('panel-page', require('./components/PanelPage.vue'));
Vue.component('alert', require('./components/Alert.vue'));
Vue.component('simulation', require('./components/Simulation.vue'));

/*Vue.component(
*    'passport-clients',
*    require('./components/passport/Clients.vue')
*);
*
*Vue.component(
*    'passport-authorized-clients',
*    require9'./components/passport/AuthorizedClients.vue')
*);
*
*Vue.component(
*    'passport-personal-access-tokens',
*    require('./components/passport/PersonalAccessTokens.vue')
*);
*/

import {DeferredReady} from './deferredReady'

Vue.use(DeferredReady)
Vue.use(Vuex)
Vue.use(AsyncComputed)

const store = new Vuex.Store({
    state: {
        alerts: []
    },
    mutations: {
        addAlert (state, data) {
            state.alerts.push({
                type: data[0],
                content: data[1]
            })
        }
    }
})

Object.defineProperty(Vue.prototype, '$bus', {
  get() {
    return this.$root.bus;
  }
})

var bus = new Vue({})

const app = new Vue({
    el: '#app',
    store,
    data: {
      bus: bus
    }
});
