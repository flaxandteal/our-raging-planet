import Vue from './app.js';
import {router} from './app.js';

export default {
  user: {
    authenticated: false,
    profile: null
  }
}
