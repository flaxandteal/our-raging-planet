import Vue from 'vue'
import App from './App.vue'
import Example from './components/Example.vue'

import VueRouter from 'vue-router'
import VueResource from 'vue-resource'

Vue.use(VueResource)
Vue.use(VueRouter)

const router = new VueRouter()

router.map({
  '/example': {
    component: Example
  }
})

router.redirect({
  '*': '/hello'
})

router.start(App, '#app')
