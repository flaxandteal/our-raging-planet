import Vue from 'vue';
import Q from 'q';
import {DeferredReadyMixin} from '../../deferredReady'
import {DeferredReady} from '../../deferredReady'

Vue.use(DeferredReady);

export default Vue.extend({
    mixins: [DeferredReadyMixin],
    created() {
        this.$lmap = null;
        this.$on('map-ready',
            function (map) {
                this.$lmap = map;
            }
        );
    },
    deferredReady() {
        this.$emit('register-component', this);
    }
})
