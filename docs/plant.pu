@startuml

skinparam class {
  BackgroundColor<<Secondary>> Wheat
}

class Student {
}
class Simulation {
  name
  settings
  centre : point
}
class Phenomenon {
  name
  color
}
class Challenge {
  text
  subtext
}
class ChallengeType {
  text
  subtext
}
class Informational {
  text
  subtext
  created
  viewed : boolean
}
class InformationalType {
  color
}
class Zone {
  extent : polygon
  attributes
}
class ZoneType {
  name
  color
  attributes_template
}
class FeatureState {
  health : numeric
  description
}
class ResourceRequirement <<Secondary>> {
  quantity : numeric
}
class ResourceType <<Secondary>> {
  name
  format : varchar
}
note bottom: format indicates print style of requirement quantity
class Feature {
  name
  address
  slug
  text : markdown
  location : point
  extent : polygon[0..1]
  wikidata : varchar[0..1]
  data_source_identifier : varchar[0..1]
}
class FeatureType {
  symbol : glyph
}
note bottom: glyph to be replaced with icon
class DataSource {
  name
  license
  uri
}

Student "1" *-- "many" Simulation

Simulation --> Phenomenon
Simulation "1" *-- "many" Challenge
Simulation "1" *-- "many" Informational
Simulation "1" *-- "many" Zone
Simulation "1" *-- "many" FeatureState

Challenge --> ChallengeType

Informational --> InformationalType

FeatureState --> Feature
FeatureState "1" *-- "many" ResourceRequirement

Feature --> FeatureType
Feature --> DataSource

Zone --> ZoneType

ResourceRequirement --> ResourceType

@enduml
