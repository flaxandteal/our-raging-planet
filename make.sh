#!/bin/sh

docker-compose -f docker-compose-artisan.yml run artisan migrate:rollback --path=vendor/serverfireteam/panel/src/database/migrations
docker-compose -f docker-compose-artisan.yml run artisan migrate:rollback

docker-compose -f docker-compose-artisan.yml run artisan migrate
docker-compose -f docker-compose-artisan.yml run artisan migrate --path=vendor/serverfireteam/panel/src/database/migrations
docker-compose -f docker-compose-artisan.yml run artisan db:seed --class='\Serverfireteam\Panel\LinkSeeder'
docker-compose -f docker-compose-artisan.yml run artisan db:seed
