

chown:
	sudo chown -R www-data ./bootstrap ./storage

ssl:
	sudo openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout containers/certificates/server.key -out containers/certificates/server.crt

vendors:
	sudo docker-compose -f docker-compose-artisan.yml run --entrypoint=composer artisan install --no-scripts 

keys:
	cp .env.example .env
	sudo docker-compose -f docker-compose-artisan.yml run artisan key:generate

db_migration:
#sudo docker-compose -f docker-compose-artisan.yml run artisan migrate:rollback --path=vendor/serverfireteam/panel/src/database/migrations
#sudo docker-compose -f docker-compose-artisan.yml run artisan migrate:rollback
	sudo docker-compose -f docker-compose-artisan.yml run artisan migrate
	sudo docker-compose -f docker-compose-artisan.yml run artisan migrate --path=vendor/serverfireteam/panel/src/database/migrations
	sudo docker-compose -f docker-compose-artisan.yml run artisan db:seed --class='\Serverfireteam\Panel\LinkSeeder'
	sudo docker-compose -f docker-compose-artisan.yml run artisan db:seed

start: chown ssl 
	sudo docker-compose up

container_workaround: vendors keys db_migration
	

