<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FeatureType extends Model
{
  public function features() {
    return $this->hasMany('App\Feature');
  }
}
