<?php

namespace App;

use Phaza\LaravelPostgis\Eloquent\PostgisTrait;
use Illuminate\Database\Eloquent\Model;

class Simulation extends Model
{
  use PostgisTrait;

  protected $postgisFields = [
    'center'
  ];

  protected $with = [
    'phenomenon'
  ];

  public function informationals() {
    return $this->hasMany('App\Informational');
  }

  public function featureStates() {
    return $this->hasMany('App\FeatureState');
  }

  public function phenomenon() {
    return $this->belongsTo('App\Phenomenon');
  }

  public function owner() {
    return $this->belongsTo('App\User', 'user_id');
  }

  public function zones() {
    return $this->hasMany('App\Zone');
  }

  public function tasks() {
    return $this->hasMany('App\Task');
  }
}
