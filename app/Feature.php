<?php

namespace App;

use Phaza\LaravelPostgis\Eloquent\PostgisTrait;
use Illuminate\Database\Eloquent\Model;

class Feature extends Model
{
  use PostgisTrait;

  protected $postgisFields = [
    'location',
    'extent'
  ];

  public function dataSource() {
    return $this->belongsTo('App\DataSource', 'data_source_id');
  }

  public function type() {
    return $this->belongsTo('App\FeatureType', 'feature_type_id');
  }
}
