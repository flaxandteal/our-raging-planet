<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FeatureState extends Model
{
  protected $with = [
    'feature'
  ];

  public function feature() {
    return $this->belongsTo('App\Feature');
  }

  public function simulation() {
    return $this->belongsTo('App\Simulation');
  }
}
