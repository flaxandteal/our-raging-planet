<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Informational extends Model
{
  public function simulation() {
    return $this->belongsTo('App\Simulation');
  }
}
