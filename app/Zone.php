<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Zone extends Model
{
  public function simulation() {
    return $this->belongsTo('App\Simulation');
  }

  public function type() {
    return $this->belongsTo('App\ZoneType', 'zone_type_id');
  ]
}
