<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Phenomenon extends Model
{
  public function simulations() {
    return $this->hasMany('App\Simulation');
  }
}
