<?php

namespace App\Http\Controllers;

use App\Simulation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Helpers\ORPSimulationHelper;
use Illuminate\Support\Facades\Input;

class SimulationController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $user = Auth::user();

        return $user->simulations;
    }

    public function show($id)
    {
        $simulation = Simulation::find($id);

        $this->authorize('simulation-view', $simulation);

        return $simulation;
    }

    public function result($id, ORPSimulationHelper $helper)
    {
        $simulation = Simulation::find($id);

        $this->authorize('simulation-view', $simulation);

        // FIXME
        //
        $window = Input::get('window');
        $time = Input::get('time');

        if (!$window || !$time) {
            abort(401, "Need windowing and time information");
        }

        $features = $simulation->featureStates->map(function ($fs) {
            $feature = $fs->feature;

            $location = [$feature->location->getLat(), $feature->location->getLng()];
            return ['id' => $fs->id, 'location' => $location, 'slug' => $feature->type->slug];
        });

        return $helper->result($simulation->id, $time, $window, $features);
    }
}
