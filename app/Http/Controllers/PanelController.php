<?php

namespace App\Http\Controllers;

use App\Simulation;
use Illuminate\Http\Request;

class PanelController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function get(int $simulationId)
    {
        /* FIXME: t-c */
        $simulation = Simulation::findOrFail($simulationId);

        return view('panel', compact("simulation"));
    }
}
