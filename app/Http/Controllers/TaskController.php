<?php

namespace App\Http\Controllers;

use App\Task;
use App\Simulation;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use \Serverfireteam\Panel\CrudController;
use Illuminate\Support\Facades\Input;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use Illuminate\Http\Request;

class TaskController extends CrudController{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function all($entity){
        parent::all($entity);

        $this->filter = \DataFilter::source(new Task);
        $this->filter->add('heading', 'Heading', 'text');
        $this->filter->add('simulation_id', 'Simulation', 'select')->options(Simulation::with('phenomenon')->get()->keyBy('id')->map(function ($s) {
            return $s->phenomenon->name . ' ' . $s->description;
        })->toArray());

        $this->filter->submit('search');
        $this->filter->reset('reset');
        $this->filter->build();

        $this->grid = \DataGrid::source($this->filter);
        $this->grid->add('heading', 'Heading');
        $this->grid->add('description', 'Description');
        $this->addStylesToGrid();

        return $this->returnView();
    }

    public function  edit($entity){

        parent::edit($entity);

        $this->edit = \DataEdit::source(new Task());

        $this->edit->label('Edit Task');

        $this->edit->add('simulation_id', 'Simulation', 'select')->options(Simulation::with('phenomenon')->get()->keyBy('id')->map(function ($s) {
            return $s->phenomenon->name . ' ' . $s->caption;
        })->toArray());

        $this->edit->add('heading', 'Heading', 'text')->rule('required');

        $this->edit->add('description', 'Description', 'textarea')->rule('required');

        return $this->returnEditView();
    }

    public function index()
    {
        if (!Input::has('simulation_id')) {
            abort(403, "We will only show tasks for a single simulation");
        }

        $simulation = Simulation::findOrFail(Input::get('simulation_id'));

        $this->authorize('simulation-view', $simulation);

        return $simulation->tasks;
    }
}
