<?php

namespace App\Http\Controllers;

use App\Simulation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class FeatureStateController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        if (!Input::has('simulation_id')) {
            abort(403, "We will only show feature states for a single simulation");
        }

        $simulation = Simulation::findOrFail(Input::get('simulation_id'));

        $this->authorize('simulation-view', $simulation);

        return $simulation->featureStates;
    }
}
