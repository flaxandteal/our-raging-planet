<?php namespace App\Helpers;

class ORPSimulationHelper
{
    protected $client;

    public function __construct(\GuzzleHttp\Client $client) {
        $this->client = $client;
        $this->host = config('orp.host');
    }

    public function result($id, $time, $window, $featureStates) {
        $response = $this->client->post('http://' . $this->host . '/result/' . $id, ['query' => [
            'time' => $time,
            'window' => $window
        ], 'json' => [
            'features' => json_encode($featureStates)
        ]]);

        return json_decode($response->getBody(), true);
    }
}
